__author__ = 'venuktangirala'

import string
import time
import pandas as pd
import logging
import nltk
import re
from nltk.corpus import stopwords
from nltk.stem.snowball import PorterStemmer
from nltk.collocations import  BigramCollocationFinder
from nltk.collocations import BigramAssocMeasures

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)


# setting in global name space , so does not need to re instanciated each time
word_re = re.compile(r'[A-Za-z]')
numbers=['0','1','2','3','4','5','6','7','8','9']
stemmer = PorterStemmer()
english_stops = set(stopwords.words('english'))
others = ['dtype','dype','object','objec']

def clenseEachReview(doc):
    """
    Parameters
    ----------
    takes a pandas series doc/review text as input
    Notes
    -----
    replace the common punctuation with space
    splits the words on space into a list
    stems the words
    Returns
    -------
    returns a pandas df of cleaned doc
    """
    doc = str(doc).lower()

    for each_pun in string.punctuation:
        doc = doc.replace(each_pun, ' ')
    for stop_word in english_stops:
        doc=doc.replace(stop_word,'')
    for num in numbers:
        doc=doc.replace(num,'')
    for other in others:
        doc.replace(other,'')

    # doc =  doc.split(' ')
    #
    # for index,word in enumerate(doc):
    #     if word.lower()  in english_stops:
    #         doc.pop(index)
    #     if word_re.match(word):
    #         doc[index] = stemmer.stem(word)

    return doc

def Main():
    """
    Parameters
    ----------
    Notes
    -----
    Returns
    -------
    """
    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/' \
               'csvFiles/yelpReview.txt'

    df = pd.read_csv(raw_data,index_col=False, sep='\t')
    tokens=[]

    for i in range(0,1000):
        info.debug('TIME_ELAPSED: %s working on :%s', time.time()-initial_time , i)
        cleaned_review = clenseEachReview(str(df.ix[i,['text']]))
        tokens += nltk.word_tokenize( cleaned_review)

    bigrams = BigramCollocationFinder.from_words(tokens)
    bigrams.apply_freq_filter(2)
    real  = bigrams.nbest(BigramAssocMeasures.likelihood_ratio,15)
    info.debug('TIME_ELAPSED: %s ', time.time()-initial_time )

    print(real)
    # fdist = nltk.FreqDist(bigrams)
    #
    # for k,v in fdist.items():
    #     print(k,v)

    return 0

if __name__ == '__main__':
    # arg1 = sys.argv[0]

    Main()


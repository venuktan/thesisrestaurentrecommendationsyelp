
__author__ = 'venuktangirala'
import pandas as pd
import matplotlib.pyplot as plt
from ggplot import *
import logging
import time

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)

def plotMatPlot(df):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """


    print(df)

    df = df.cumsum()

    plt.plot(df['k'], df['RMSE_original'], 'ro', df['k'], df['RMSE_original'], 'b--')
    plt.plot(df['k'], df['TCBF'], 'rx', df['k'], df['TCBF'], 'b--')
    plt.axis('on')
    plt.show()

    return 0

def ggPlotting(df):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """
    # lables = df.ix[:,['RMSE']].apply(str).values
    data = pd.melt(df[['k','Collaborative Filtering','TCBF']],id_vars='k')

    print ggplot(aes(x='k',y='value',colour='variable'),data=data)+ \
          geom_point() + geom_line() + xlab('K')+ ylab('RMSE')


    # print ggplot(aes(x='k',y='RMSE_original') ,aes(x='k',y='RMSE_enhanced'),data=df)+ \
    #     geom_point(color='blue',)+ geom_line(color='red')

    return 0

def cal_improvement(df):
    """about the method

    :param var1: describe var1
    :returns out1: describe out1
    :returns out2: describe out2
    """
    df['improvement'] = df['Collaborative Filtering']-df['TCBF']
    print df
    df.to_csv("../data/RMSE_k.csv", sep=',')
    return 0

def cal_improvement_percent(df):
    """about the method

    :param var1: describe var1
    :returns out1: describe out1
    :returns out2: describe out2
    """
    df['improvement_percent'] = df['improvement']*200
    df.to_csv("../data/RMSE_k.csv", sep=',', index=False)
    return 0

if __name__ == '__main__':
    # arg1 = sys.argv[0]
    df = pd.read_csv("../data/RMSE_k.csv", sep=',')
    # plotMatPlot(df)
    ggPlotting(df)
    # cal_improvement(df)
    # cal_improvement_percent(df)
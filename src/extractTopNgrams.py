import time

__author__ = 'venuktangirala'

import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
import logging

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT, filename='../logs/runnnnn.log')
info.setLevel(logging.DEBUG)

def Main():
    """
    Parameters
    ----------

    Notes
    -----

    Returnsx
    -------
    """
    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt'

    df=pd.read_csv(raw_data,index_col=False, sep='\t')

    list_of_docs = []
    for i in range(0,1000):
        info.debug('TIME_ELAPSED: %s working on :%s', time.time()-initial_time , i)

        list_of_docs.append(str( df.ix[i,['text']] ))

    n_grams=CountVectorizer(ngram_range=(1, 5), stop_words='english')
    X=n_grams.fit_transform(list_of_docs)

    print( n_grams.get_feature_names() )

    return 0

if __name__ == '__main__':
    # arg1 = sys.argv[0]
    Main()

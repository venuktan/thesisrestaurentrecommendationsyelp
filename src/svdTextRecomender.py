from divisi2 import SparseMatrix

__author__ = 'venuktangirala'

import cPickle
from csc import divisi2
import pandas as pd
import os,data.menuItemsByCusine
import time
import logging
info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s() ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)


indian = open(os.path.join(data.menuItemsByCusine.__path__[0], 'indian.txt')).read()
indian = [line.lower() for line in indian.split('\n') if line.strip() != '']
menu_items=indian
unique_items = {}


def preProcessBuildModel(raw_data):
    """
    Parameters
    ----------
    raw pandas df of review with all columns
    Notes
    -----
    load the data
    Returns
    -------
    0 on success
    """
    data_frame = pd.read_csv(raw_data, index_col=False, sep='\t')
    data_frame['index'] = data_frame.index.values
    info.debug(' Pandas DF build form raw data TIME_ELAPSED : %s', time.time()-initial_time )

    # ratings_data_frame = data_frame.ix[:,['user_id','business_id','stars']]
    # review_data_frame = data_frame.ix[:, ['user_id', 'business_id', 'text']]

    concise_data_frame = data_frame.ix[:,['stars','text','user_id','business_id','index']]

    df_item_user_id, df_item_business_id = exrtactMenuItemsHelper(concise_data_frame)
    info.debug('DF of item by user_id built \tTIME_ELAPSED: %s\n %s',time.time()-initial_time , df_item_user_id)
    info.debug('DF of item by business_id built \tTIME_ELAPSED:%s\n %s', time.time()-initial_time ,df_item_business_id)

    # building sparse matrix for item X userid
    sparse_matrix_item_user_id = divisi2.make_sparse( df_item_user_id.ix[:,[0,1,2]].values)
    info.debug('sparse_matrix_item_user_id built \tTIME_ELAPSED:\%sn %s', time.time()-initial_time ,sparse_matrix_item_user_id)

    # building sparse matrix item X business_id
    sparse_matrix_item_business_id = divisi2.make_sparse( df_item_business_id.ix[:,[0,1,2]].values )
    info.debug('sparse_matrix_item_business_id build \tTIME_ELAPSED:%s\n %s', time.time()-initial_time ,sparse_matrix_item_business_id)

    # sparse mat locations
    item_user_id_sparce_matrix_model = '/Users/venuktangirala/PycharmProjects/thesis/models/item_user_id_sparce_matrix.pickle'
    item_business_id_sparce_matrix_model = '/Users/venuktangirala/PycharmProjects/thesis/models/item_business_id_sparce_matrix.pickle'

    with  open(item_user_id_sparce_matrix_model,'wb') as user_model_file:
        cPickle.dump(sparse_matrix_item_user_id, user_model_file, cPickle.HIGHEST_PROTOCOL)
        info.debug('user_model_file written to\tTIME_ELAPSED:%s\n %s', time.time()-initial_time ,item_user_id_sparce_matrix_model)

    with  open(item_business_id_sparce_matrix_model,'wb') as business_model_file:
        cPickle.dump(sparse_matrix_item_business_id, business_model_file, cPickle.HIGHEST_PROTOCOL)
        info.debug('business_model_file written to \tTIME_ELAPSED:%s\n %s', time.time()-initial_time ,item_business_id_sparce_matrix_model)

    print(sparse_matrix_item_user_id)
    print(sparse_matrix_item_business_id)

    return 0

def exrtactMenuItemsHelper(data_frame):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    returns a list of tuples in the format [(1*ratings,user_id/business_id,menu_item),
                                            (1*ratings,user_id/business_id,menu_item),..]
    """
    df_items_by_user_id = pd.DataFrame()
    df_items_by_business_id = pd.DataFrame()

    info.debug('building pandas df of tuples now \tTIME_ELAPSED: %s', time.time()-initial_time )

    for row_num in range(0,data_frame.__len__()):

        if(row_num % 5000==0 and row_num != 0):
            info.debug('done building DF of size :%s\tTIME_ELAPSED: %s \n', row_num, time.time()-initial_time)

        tmp_list_user_id, tmp_list_business_id = extractMenuItems(data_frame.ix[row_num,:])
        df_items_by_user_id = df_items_by_user_id.append(tmp_list_user_id)
        df_items_by_business_id = df_items_by_business_id.append(tmp_list_business_id)

    return df_items_by_user_id, df_items_by_business_id


def extractMenuItems(df):
    """
    Parameters
    ----------
    takes a document as input
    Notes
    -----

    Returns
    -------

    """
    df_items_by_user_id = pd.DataFrame()
    df_items_by_business_id = pd.DataFrame()

    text = str(df[['text']])

    for item in menu_items:
        if item not in text.lower():
            continue
        else:
            unique_items[item] = unique_items.get(item,0)+1
            df_items_by_user_id = df_items_by_user_id.append( [[ 1 * int(df[['stars']]), str(df[['user_id']]), str(item) ]] )
            df_items_by_business_id = df_items_by_business_id.append( [[1 * int(df[['stars']]), str(df[['business_id']]), str(item) ]] )

    return df_items_by_user_id, df_items_by_business_id

if __name__ == '__main__':
    # arg1 = sys.argv[0]

    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt'
    info.debug('starting TIME_ELAPSED %s',time.time()-initial_time)

    preProcessBuildModel(raw_data)
    info.debug(' Model Built and written to disk TIME_ELAPSED : %s', time.time()-initial_time )

    print("===================================================")
    print("unique item:")
    print(unique_items)

    with open('/Users/venuktangirala/PycharmProjects/thesis/models/unique_items.txt','w') as file:
        file.write(unique_items)

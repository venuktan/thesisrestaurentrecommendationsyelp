__author__ = 'venuktangirala'
from textblob import TextBlob
class KeyWordExtract:
    def __init__(self):
        pass
    def key_word_extract(self,review_list):
        """
        review_list: list of textual reviews
        nouns_per_review: the nouns in the reviews
        """
        nouns_per_review=[]
        for r in review_list:
            blob = TextBlob(r)
            nouns_per_review.append(blob.noun_phrases)

        return nouns_per_review

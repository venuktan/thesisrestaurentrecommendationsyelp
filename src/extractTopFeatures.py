import math
from text.blob import TextBlob
import pandas as pd

import logging
import time

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)

def tf(word, blob):
    return blob.words.count(word) / len(blob.words)

def n_containing(word, bloblist):
    return sum(1 for blob in bloblist if word in blob)

def idf(word, bloblist):
    return math.log(len(bloblist) / (1 + n_containing(word, bloblist)))

def tfidf(word, blob, bloblist):
    return tf(word, blob) * idf(word, bloblist)

def main():
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """

    bloblist = getListDoc()

    for i, blob in enumerate(bloblist):
        print("Top words in document {}".format(i + 1))
        scores = {word: tfidf(word, blob, bloblist) for word in blob.words}
        sorted_words = sorted(scores.items(), key=lambda x: x[1], reverse=True)
        for word, score in sorted_words[:3]:
            print("\tWord: {}, TF-IDF: {}".format(word, round(score, 5)))

    return 0

def getListDoc():
    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt'

    data_frame = pd.read_csv(raw_data, index_col=False, sep='\t')

    doc_list =[]
    for row_num, row in data_frame.iterrows():
        info.debug('TIME_ELAPSED: %s \t working on doc#: %s', time.time()-initial_time, row_num)
        doc_list.append(TextBlob(str(row['text'])).lower())

    info.debug('TIME_ELAPSED: %s\t ALL Documents done ', time.time()-initial_time )

    return doc_list

if __name__ == '__main__':
    main()
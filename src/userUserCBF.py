__author__ = 'venuktangirala'

import pandas as pd
import numpy as np
import os
import cPickle
import time
from sklearn.metrics import pairwise_distances
import logging

def userUserCBF():

    model_lcation = './models/model1.model'
    # raw_data = preProcessData()

    # reading all the objects from disk to memory
    with open(model_lcation) as model:
        userId_businessId_stars = cPickle.load(model)
        businessList = cPickle.load(model)
        usersList = cPickle.load(model)
        logging.warning('all model files loaded')

    print("# of business={} \t #of users ={}" .format(len(businessList),len(usersList) ) )
    # print(userId_businessId_stars.ix[1:10,['mean']])
    computeSimilarities(userId_businessId_stars)

def computeSimilarities(userId_businessId_stars):

    logging.warning('in computeSimilarities')

    userMeanCenteredRatings = userId_businessId_stars.sub(userId_businessId_stars['mean'], axis='index')

    logging.warning('mean centered ratings computed')

    index = userMeanCenteredRatings.index

    cosineSimilarities = pairwise_distances(userMeanCenteredRatings, metric='cosine')

    logging.warning('cosine similarities computed')

    print(cosineSimilarities)
    # cosineSimilaritiesDataFrame = pd.DataFrame(cosineSimilarities,index=index,columns=index)
    #
    # print(cosineSimilaritiesDataFrame.ix[1:10,1:10])


def preProcessData():
    """
    load the file original YelpReview.csv file from disk and clean the data frame
    create a ratings userId_businessId_stars with user_id as rows and business_id as
        columns and stars a values
    Computes user centered mean rating
    writes the userId_businessId_stars to disk
    writes a cPickle with all the objects to disk

    Parameters
    ----------
    calling function does need to pass anything just make sure the yeloReview file is in place

    Returns
    -------
    path of the pickled file with all objects
    """
    # reading the whole file from disk and parsing the date field as date
    dataFrame = pd.read_csv('/Users/venuktangirala/data/'
                                  'yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt',
                                  sep='\t', parse_dates=True)

    # changing the data type of non string columns
    dataFrame['stars'] = dataFrame['stars'].astype(np.float16)
    dataFrame['votes_cool'] = dataFrame['votes_cool'].astype(np.int16)
    dataFrame['votes_funny'] = dataFrame['votes_funny'].astype(np.int16)
    dataFrame['votes_useful'] = dataFrame['votes_useful'].astype(np.int16)

    # getting a list actually a set of all user ids and business ids
    businessList =  [ list(x) for x in dataFrame.ix[:,['business_id']].T.itertuples() ][0]
    businessList = set(businessList)
    usersList = [ list(x) for x in dataFrame.ix[:,['user_id']].T.itertuples() ][0]
    usersList= set(usersList)

    userId_businessId_stars = dataFrame.ix[:, ['user_id', 'business_id', 'stars']]
    ratingsDataFrame = pd.pivot_table(userId_businessId_stars, rows='user_id', cols='business_id')

    #building pandas dataframe of the users mean-centered rating vectors, indexed by users
    ratingsDataFrame['mean'] = ratingsDataFrame.mean(axis=1)

    # replacing the NaN values with 0 in the data frame
    # ratingsDataFrame.fillna(0, inplace=True)

    # writing the rating data frame to disk
    # ratingsDataFrame.to_csv('./ratingsDataFrame.csv', sep=',')

    # creating a directory for models if not present
    modelDir = './models'
    if not os.path.exists(modelDir):
        os.makedirs(modelDir)
    model_lcation = modelDir+'/model1.pickle'

    pickleFile = open(model_lcation,'wb')

    # writing all the objects created in this method to pickle file
    # have to read the objects in the same order as written
    cPickle.dump(ratingsDataFrame, pickleFile, cPickle.HIGHEST_PROTOCOL)
    cPickle.dump(businessList, pickleFile, cPickle.HIGHEST_PROTOCOL)
    cPickle.dump(usersList, pickleFile, cPickle.HIGHEST_PROTOCOL)

    return model_lcation

if __name__ == '__main__':
    start_time = time.time()
    logging.warning('entering UU-CBF')
    userUserCBF()
    print "run time =",time.time() - start_time, "seconds"

import os
import cPickle
import math

__author__ = 'venuktangirala'
import logging
import time
import pandas as pd
from csc import divisi2
import models
import numpy as np

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)

def Main():
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """
    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt'

    info.debug('TIME_ELAPSED: %s read file : %s into pandas DF', time.time()-initial_time, raw_data)

    data_frame = pd.read_csv(raw_data, index_col=False, sep='\t')
    info.debug(' Pandas DF build form raw data TIME_ELAPSED : %s', time.time() - initial_time)

    data_frame = data_frame.ix[:, ['stars', 'user_id', 'business_id']]

    # a DF to hold the k and rmse
    list_of_no_features = [50,100, 200, 250, 300, 350, 400, 500, 600, 900,1000,1500,2000,5000]
    # list_of_no_features = [5000]
    metrics_k = pd.DataFrame(index=list_of_no_features, columns=['k', 'RMSE'])
    metrics_k['k'] = list_of_no_features
    # metrics_k = metrics_k.fillna(0)

    for index, row in metrics_k.iterrows():
        U_S_V_model_loc = "../models/plainSVD_Models/U_S_V_"+ str(row['k']) +".pickle"
        # sparse_matrix_model_loc, U_S_V_model_loc = buildModelUSV(data_frame,row['k'])

        info.debug('TIME_ELAPSED: %s WORKING ON METRIC FOR K=%s', time.time()-initial_time, str(row['k']) )
        # recommender_loc = buildModelRecommender(U_S_V_model_loc, str(row['k']) )
        info.debug('TIME_ELAPSED: %s FINISHED RECOMMENDER FOR K=%s', time.time()-initial_time, str(row['k']) )

        recommender_loc = "../models/plainSVD_Models/recommender_" + str(row['k']) + ".pickle"

        rated_data_frame = rateAllTestAndEval(recommender_loc, data_frame)
        info.debug('TIME_ELAPSED: %s rateAllTestAndEval DONE FOR K=%s', time.time()-initial_time, str(row['k']))

        rmse = calculateRMSE(rated_data_frame)

        metrics_k.ix[index,['RMSE']] = rmse
        print("=======================================================================================================")
        print(" RMSE={} for k={}".format( rmse,str(row['k']) ))
        print("=======================================================================================================")


    info.debug('TIME_ELAPSED: %s ALL K RMSE done', time.time()-initial_time )

    print("=======================================================================================================")
    print(metrics_k)
    print("=======================================================================================================")

    return 0

def calculateRMSE(data_frame):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """

    sum_of_squared_erros = 0

    for index , row in data_frame.iterrows():
        sum_of_squared_erros+=(row['stars']-row['predictedStars'])**2

        if index%1000 ==0 and index!=0:
            info.debug('TIME_ELAPSED: %s done squaring : %s ', time.time()-initial_time , index)

    avg_of_squared_erros = sum_of_squared_erros/data_frame.shape[0]

    return math.sqrt(avg_of_squared_erros)

def rateAllTestAndEval(recommender_loc,data_frame):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """
    with open(recommender_loc, "rb") as file:
        recommender = cPickle.load(file)
        info.debug('TIME_ELAPSED: %s recommender loaded', time.time()-initial_time )

    with open("../models/trainTestSplit/trainTestSplit.pickle", "rb") as file:
        train_set_rows = cPickle.load(file)
        test_set_rows  = cPickle.load(file)
        info.debug('TIME_ELAPSED: %s train_set_rows & test_set_rows loaded', time.time()-initial_time )

    rated_data_frame = data_frame.ix[test_set_rows]
    rated_data_frame['predictedStars'] = np.nan

    for index, row in rated_data_frame.iterrows():
        rated_data_frame.ix[index,['predictedStars']]= recommender.entry_named(row['user_id'], row['business_id'])

        if index%1000==0 and index!=0:
            info.debug('TIME_ELAPSED: %s Done :%s ', time.time()-initial_time , index)

    rated_data_frame['predictedStars'] = np.round(rated_data_frame['predictedStars'],decimals=1)

    info.debug('TIME_ELAPSED: %s All predictions done ', time.time()-initial_time )

    # rated_data_frame.to_csv('../data/predicted_k_300.csv')
    # info.debug('TIME_ELAPSED: %s rated_data_frame written to disk', time.time()-initial_time )

    return rated_data_frame

def buildModelRecommender(U_S_V_model_loc ,k):
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """
    # sample_userid = "rLtl8ZkDX5vH5nAx9C3q5Q"
    # sample_busid = "6oRAC4uyJCsJl1X0WZpVSA"

    with open(U_S_V_model_loc,"rb") as file1:
        u= cPickle.load(file1)
        s= cPickle.load(file1)
        v= cPickle.load(file1)
        row_shift= cPickle.load(file1)
        col_shift = cPickle.load(file1)
        total_shift = cPickle.load(file1)

    info.debug('TIME_ELAPSED: %s  U_S_V loaded from disk', time.time()-initial_time )

    recommender = divisi2.reconstruct(u, s, v,shifts = (row_shift,col_shift, total_shift))

    info.debug('TIME_ELAPSED: %s  recommender BUILT', time.time()-initial_time )

    recommender_loc = "../models/plainSVD_Models/recommender_"+k+".pickle"
    with open(recommender_loc,"wb") as file2:
        cPickle.dump(recommender,file2, cPickle.HIGHEST_PROTOCOL)

    # with open("../models/plainSVD_Models/recommender_300_non_normalized.pickle","rb") as file:
    #     recommender= cPickle.load(file)

    # print(recommender.entry_named(sample_userid,sample_busid))

    return recommender_loc

def buildModelUSV(data_frame, no_features):
    info.debug('TIME_ELAPSED: %s building model now ', time.time()-initial_time )

    sparse_matrix_model_loc ="../models/plainSVD_Models/sparseMatrix.pickle"

    # uncomment the below line only id you want to re build the sparse_matrix
    # buildSparseMatrix(data_frame)

    with open(sparse_matrix_model_loc) as file:
        sparse_matrix=cPickle.load(file)
    info.debug('TIME_ELAPSED: %s Read sparse matrix from disk %s', time.time()-initial_time, sparse_matrix)

    info.debug('TIME_ELAPSED: %s making U S V ', time.time()-initial_time )

    sparse_matrix,row_shift, col_shift, total_shift =sparse_matrix.mean_center()

    info.debug('TIME_ELAPSED: %s MEAN CENTERING done', time.time()-initial_time )

    u,s,v = sparse_matrix.svd(k=no_features)

    info.debug('TIME_ELAPSED: %s U S V built', time.time()-initial_time )

    U_S_V_model_loc = "../models/plainSVD_Models/U_S_V_"+str(no_features)+".pickle"

    with open(U_S_V_model_loc,"wb") as file:
        cPickle.dump(u, file, cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(s, file, cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(v, file, cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(row_shift,file,cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(col_shift,file,cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(total_shift, file, cPickle.HIGHEST_PROTOCOL)

    info.debug('TIME_ELAPSED: %s U_S_V  written to disk ', time.time()-initial_time)

    return sparse_matrix_model_loc, U_S_V_model_loc

def buildSparseMatrix(data_frame):
    sparse_matrix = divisi2.make_sparse(data_frame[['stars', 'user_id', 'business_id']].values)

    info.debug('TIME_ELAPSED: %s sparse matrix build ', time.time() - initial_time)

    # write the sparse matrix to disk
    plainSVD_dir = "../models/plainSVD_Models"
    if not os.path.exists(plainSVD_dir):
        os.makedirs(plainSVD_dir)
        info.debug('directory %s created', plainSVD_dir)

    sparse_matrix_model_loc = plainSVD_dir + "/sparseMatrix.pickle"

    with open(sparse_matrix_model_loc,'wb') as out:
        cPickle.dump(sparse_matrix, out, cPickle.HIGHEST_PROTOCOL)

    info.debug('TIME_ELAPSED: %s wrote sparse_matrix to disk ', time.time() - initial_time)

    return 0


if __name__ == '__main__':
    # arg1 = sys.argv[0]
    info.debug('TIME_ELAPSED: %s ', time.time()-initial_time )

    Main()
from idlelib.ReplaceDialog import replace
import cPickle

__author__ = 'venuktangirala'
import logging
import time
import pandas as pd
import numpy as np

info = logging.getLogger('INFO')
initial_time = time.time()
FORMAT = "[INFO: %(filename)s:%(lineno)s - %(funcName)20s ] %(message)s"
logging.basicConfig(format=FORMAT)
info.setLevel(logging.DEBUG)

def Main():
    """
    Parameters
    ----------

    Notes
    -----

    Returns
    -------
    """

    raw_data = '/Users/venuktangirala/data/yelp_phoenix_academic_dataset/csvFiles/yelpReview.txt'

    info.debug('TIME_ELAPSED: %s read file : %s into pandas DF', time.time() - initial_time, raw_data)

    data_frame = pd.read_csv(raw_data, index_col=False, sep='\t')
    data_frame['index'] = data_frame.index.values
    info.debug(' Pandas DF build form raw data TIME_ELAPSED : %s', time.time() - initial_time)

    data_frame = data_frame.ix[:, ['stars', 'text', 'user_id', 'business_id', 'index']]

    train_set_size  =183926
    test_set_size  =45982

    train_set_rows = np.random.choice(data_frame.index, int(train_set_size), replace=False)
    test_set_rows =  np.random.choice(data_frame.index, int(test_set_size), replace=False)

    file = "../models/trainTestSplit/trainTestSplit.pickle"

    with open(file,"wb") as file:
        cPickle.dump(train_set_rows,file, cPickle.HIGHEST_PROTOCOL)
        cPickle.dump(test_set_rows,file,cPickle.HIGHEST_PROTOCOL)
        info.debug('TIME_ELAPSED: %s train_set_rows and test_set_rows written to disk ', time.time()-initial_time )

    return 0

if __name__ == '__main__':
    # arg1 = sys.argv[0]
    Main()
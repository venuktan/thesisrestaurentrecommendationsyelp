
# coding: utf-8

# In[1]:

# from csc import divisi2
import numpy as np
import pandas as pd
from scipy.sparse import * 
import scipy 
from scipy import linalg
# from scipy.sparse import linalg
from scipy.spatial import distance
from copy import deepcopy
import functools 


# In[18]:

A = np.array([
             [3.0,4.0,5.0,0],
             [4.0,5.0,0,3.0],
             [2.0,1.0,5.0,5.0],
             [2.0,0,0,5.0]
             ]
             )


# In[19]:

print A
print "shape of A: {}".format(A.shape)


# In[20]:

U,s,V = linalg.svd(A) #compute SVD


# In[21]:

print U
print "shape of U: {}".format(U.shape)


# In[22]:

print s
print "shape of s or Sigma: {}".format(s.shape)


# In[23]:

print V
print "shape of V: {}".format(V.shape)


# In[24]:

sigma = np.diag(s)


# In[25]:

sigma 


# In[26]:

A_prime = np.dot(U,np.dot(sigma,V))
A_prime


# In[27]:

# user user collabrative filtering
def UUCF(mat,i,j): 
    score_i_j_mat =0
    rows, cols = np.shape(mat)
    
    for index in range(0,rows-1): # interate over rows/users
        if(index == i): #skipping the row/user in question
            continue

        # w or Weight is the similarity of 2 user vectors 
        w=1-scipy.spatial.distance.cosine(mat[i,:],mat[index,:])
        other_index_j = mat[index,j]
        
        #print "similarity= {}, other_index_j= {}".format(w,other_index_j)
        score_i_j_mat += other_index_j * w
        
    return score_i_j_mat


# In[28]:

# item item collabrative filtering
def IICF(mat,i,j):
    score_i_j_mat =0
    rows, cols = np.shape(mat)
    
    for index in range(0,cols-1): # interate over col/item
        if(index == j): #skipping the col/item in question
            continue
        
        # w or Weight is the similarity of 2 item vectors 
        w= 1-scipy.spatial.distance.cosine(mat[:,j],mat[:,index])
        other_i_index = mat[i,index]
        
        #print "similarity= {}, other_i_index= {}".format(w,other_i_index)
        score_i_j_mat += other_i_index * w
        
    return score_i_j_mat


# In[29]:

U[2,0]= UUCF(U,2,0)
print U[2,0]

U[2,1]= UUCF(U,2,1)
print U[2,1]

U[5,0]= UUCF(U,5,0)
print U[5,0]

U[5,1]= UUCF(U,5,1)
print U[5,1]


# In[30]:

V[0,4] = IICF(V, 0,4)
print V[0,4]

V[0,5] = IICF(V, 0,5)
print V[0,5]

V[1,4] = IICF(V, 1,4)
print V[1,4]

V[1,5] = IICF(V, 1,5)
print V[1,5]


# In[31]:

print U
print "shape of U: {}".format(U.shape)


# In[32]:

print V
print "shape of V: {}".format(V.shape)


# In[33]:

A_computed = np.dot(U,np.dot(sigma,V))
A_computed= np.round(A_computed, decimals=1)
print A_computed


# In[34]:

A_UUCF=A
A_UUCF[0,3]= UUCF(A,0,3)
A_UUCF[1,2]= UUCF(A,1,2)
A_UUCF[3,1]= UUCF(A,3,1)
A_UUCF[3,2]= UUCF(A,3,2)
A_UUCF


# In[34]:




# In[35]:

A_IICF=A
A_IICF[0,3]= IICF(A,0,3)
A_IICF[1,2]= IICF(A,1,2)
A_IICF[3,1]= IICF(A,3,1)
A_IICF[3,2]= IICF(A,3,2)
A_IICF


# In[20]:

USS= np.zeros((4,4))
for this_user in range(0,np.shape(A)[0]):
    for other_user in range(0,np.shape(A)[0]):
        w=1-scipy.spatial.distance.cosine(A[this_user, :],A[other_user,])
        USS[this_user,other_user]=w


# In[21]:

USS


# In[22]:

VSS= np.zeros((4,4))
for this_item in range(0,np.shape(A)[0]):
    for other_item in range(0,np.shape(A)[0]):
        w=1-scipy.spatial.distance.cosine(A[:, this_item],A[:,other_item])
        VSS[this_item,other_item]=w


# In[23]:

VSS


# In[24]:

A_prime_prime = np.dot(USS,np.dot(sigma,VSS))
A_prime_prime


# In[24]:




# ## Final recommendation score from thesis

# In[25]:

user_item = np.array([
             [2.0,1.0,4.0,2.0,4.0],
             [3.0,4.0,3.0,5.0,2.0],
             [1.0,4.0,1.0,1.0,2.0],
             [4.0,3.0,4.0,2.0,4.0]
             ]
             )


# In[26]:

business_item = np.array([
             [4.0,3.0,4.0,5.0,2.0],
             [1.0,2.0,3.0,2.0,2.0],
             [4.0,1.0,4.0,2.0,4.0],
             [4.0,5.0,2.0,5.0,2.0]
             ]
             )


# In[27]:

user_bus_sim = np.empty([np.shape(user_item)[0], np.shape(business_item)[0]])
for user in range(0,np.shape(user_item)[0]):
    for bus in  range(0,np.shape(business_item)[0]):
        sim = w=1-scipy.spatial.distance.cosine(user_item[user, :],business_item[bus,:])
        print "sim : {}".format(sim)
        user_bus_sim[user,bus] = sim

user_bus_sim = np.around(user_bus_sim,2)
pd.DataFrame(user_bus_sim*5, columns=['Business1','Business2','Business3','Business4'], index=['User1','User2','User3','User4'])


# In[51]:


def row_wise_simi(mat1, mat2):
    # user and business similarity
    user_bus_sim = np.empty([np.shape(mat1)[0],np.shape(mat2)[0]])
    for user in range(0,np.shape(mat1)[0]):
        for bus in  range(0,np.shape(mat2)[0]):
            sim = w=1-scipy.spatial.distance.cosine(mat1[user, :],mat2[bus,:])
            print "sim : {}".format(sim)
            user_bus_sim[user,bus] = sim
    return user_bus_sim


# In[40]:

Bus = np.array([
             [4.0,4.5,4.0,5.0],
             [0,5.0,5.0,0],
             [0,2.0,2.0,2.0],
             ]
             )
pd.DataFrame(Bus,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['Bus1','Bus2','Bus3'])


# In[41]:

Bus_iicf = deepcopy(Bus)
Bus_iicf [1,0]= IICF( Bus,1,0)
Bus_iicf [1,3]= IICF( Bus,1,3)
Bus_iicf [2,0]= IICF( Bus,2,0)
Bus_iicf = np.round( Bus_iicf, decimals=2)
pd.DataFrame( Bus_iicf,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['Bus1','Bus2','Bus3'])


# In[42]:

Bus_uucf = deepcopy(Bus)
Bus_uucf [1,0]= UUCF( Bus,1,0)
Bus_uucf [1,3]= UUCF( Bus,1,3)
Bus_uucf [2,0]= UUCF( Bus,2,0)
Bus_uucf = np.round(  Bus_uucf, decimals=2)
pd.DataFrame(  Bus_uucf,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['Bus1','Bus2','Bus3'])


# ## Business Averaged Matrix

# In[43]:

bus_avg= (Bus_iicf+Bus_uucf)/2
pd.DataFrame(bus_avg  ,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['Bus1','Bus2','Bus3'])


# ## User matrix

# In[44]:

User = np.array([
             [4.0,4.0,4.0,0],
             [0,5.0,5.0,0],
             [0,3.5,2.0,3.5],
             ]
             )
pd.DataFrame(User,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['User1','User2','User3'])


# In[45]:

User_iicf = deepcopy(User)
User_iicf [1,0]= IICF( User,1,0)
User_iicf [0,3]= IICF( User,0,3)
User_iicf [1,3]= IICF( User,1,3)
User_iicf [2,0]= IICF( User,2,0)
User_iicf = np.round( User_iicf, decimals=2)
pd.DataFrame( User_iicf,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['User1','User2','User3'])


# In[46]:

User_uucf = deepcopy(User)
User_uucf [1,0]= UUCF( User_uucf,1,0)
User_uucf [0,3]= UUCF( User_uucf,0,3)
User_uucf [1,3]= UUCF( User_uucf,1,3)
User_uucf [2,0]= UUCF( User_uucf,2,0)
User_uucf = np.round( User_uucf, decimals=2)
pd.DataFrame( User_uucf,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['User1','User2','User3'])


# ## User Averaged

# In[47]:

user_avg= (User_iicf+User_uucf)/2


pd.DataFrame(np.around(user_avg,2) ,columns=['Chicken','Sandwich','Pesto','Veggie'], index=['User1','User2','User3'])


# In[54]:

userAvg_busAvg_sim = row_wise_simi(user_avg, bus_avg)
userAvg_busAvg_sim= userAvg_busAvg_sim*5
userAvg_busAvg_sim
pd.DataFrame(np.around(userAvg_busAvg_sim,2),columns=['Business1','Business2','Business3'], index=['User1','User2','User3'])


# In[50]:

a=np.array([1,2,3])
b=np.array([2,4,0])
c=np.array([4,4,3])
top=np.dot(c,b)
bottom = np.linalg.norm(c)*np.linalg.norm(b)
w=top/bottom
w





__author__ = 'venuktangirala'

import cPickle
import string
from src.svdTextRecomender import info
from os import sys,path
sys.path.append(path.dirname((path.dirname(__file__))))

import nltk
import re
from csc import divisi2
from nltk.stem import *
from nltk.corpus import stopwords
import pandas as pd

def normalized_words(fields):
    doc = nltk.corpus.brown.words(fields)
    doc = [
        word for word in doc
        if word.lower() not in english_stops
        ]

    for word in doc :
        if word_re.match(word):
            yield stemmer.stem(word)

if __name__ == '__main__':
    # arg1 = sys.argv[0]
    word_re =  re.compile(r'[A-Za-z]')
    stemmer = PorterStemmer()
    english_stops = set(stopwords.words('english'))

    print(english_stops)

    fields = nltk.corpus.brown.fileids(categories=['news'])

    entries = ((1, term, doc)
               for doc in fields
               for term in normalized_words(doc))
    info.debug('hhh')
    matrix = divisi2.make_sparse(entries)

    print(matrix)

def cleanEachReviewHelper(data_frame, review_cleaned_model_location):
    """
    Parameters
    ----------
    data_frame: pandas df of user_id , business_id, reviews
    review_cleaned_model_location : location of the model store location
    Notes
    -----
    parses thru all the reviews i.e. calls clenseEachReview on each review
    writes the cleaned df of reviews to disk
    Returns
    -------
    0 on success
    """
    review_cleaned_model = open(review_cleaned_model_location, 'wb')

    data_frame[['text']] = data_frame[['text']].apply(clenseEachReview, axis=1)

    cPickle.dump(data_frame, review_cleaned_model, cPickle.HIGHEST_PROTOCOL)
    return 0

# setting in global name space , so does not need to re instanciated each time
word_re = re.compile(r'[A-Za-z]')
stemmer = PorterStemmer()
english_stops = set(stopwords.words('english'))

def clenseEachReview(doc):
    """
    Parameters
    ----------
    takes a pandas series doc/review text as input
    Notes
    -----
    replace the common punctuation with space
    splits the words on space into a list
    stems the words
    Returns
    -------
    returns a pandas df of cleaned doc
    """
    doc = str(doc)

    for each_pun in string.punctuation:
        doc = doc.replace(each_pun, ' ')

    doc =  doc.split(' ')
    info.debug('working on',doc)
    documtent_num=+1
    for index,word in enumerate(doc):
        if word.lower()  in english_stops:
            doc.pop(index)
        if word_re.match(word):
            doc[index] = stemmer.stem(word)
    return pd.DataFrame(doc)

